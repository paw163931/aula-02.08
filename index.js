function numeroaleatorio () {
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            const numero = Math.floor(Math.random()*10)+1;

            if (numero <=5) {
                resolve(numero);
            } else {
                reject(new Error("o número é maior que 5"))
            }

        }, 3000)
    })
}

numeroaleatorio ()
.then(numero => console.log("Sucesso! o numero é:" + numero))
.catch (error => console.log("ERRO!" + error.message));
